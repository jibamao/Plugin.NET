﻿namespace PluginNET.error
{
    /// <summary>
    /// 加载插件时发生错误的错误类型
    /// </summary>
    public enum PluginErrorTypes
    {
        /// <summary>
        /// 没有错误
        /// </summary>
        None,

        /// <summary>
        /// 插件不是有效的托管dll文件
        /// </summary>
        InvalidManagedDllFile,

        /// <summary>
        /// 无法从程序集中加载类的类型定义，这可能是dll依赖的文件不存在引起的
        /// </summary>
        CannotLoadClassTypes,

        /// <summary>
        /// 插件内未找到插件类，这可能是类未以指定前缀命名，或者类型不是class，或类在程序集外不可访问
        /// </summary>             
        PluginClassNotFound,
        /// <summary>
        /// 未知错误
        /// </summary>
        Unkown,
    }
}
