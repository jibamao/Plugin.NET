﻿using System;
using System.Reflection;

namespace PluginNET.events
{
    /// <summary>
    /// 事件参数的基类
    /// </summary>
    [Serializable]
    public class PluginEventArgs : EventArgs
    {
        /// <summary>
        /// 取消继续执行操作，设置为true为阻止本次事件后面的代码继续执行
        /// </summary>
        public bool Cancel { get; set; }
        /// <summary>
        /// DLL文件的完整路径
        /// </summary>
        public string FileName { get; internal set; }
        /// <summary>
        /// 加载的程序集对象
        /// </summary>
        public Assembly Assembly { get; internal set; }
        /// <summary>
        /// 加载插件类成功后读取到的类的类型
        /// </summary>
        public Type ClassType { get; internal set; }
    }
}
