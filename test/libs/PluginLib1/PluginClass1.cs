﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginLib1
{
    public class PluginClass1 : PluginInterface
    {
        public override void Load()
        {
            Console.WriteLine("Load" + this.GetType().FullName);
        }

        public override string Method1()
        {
            return "这是从第一个插件里面来的数据";
        }

        public override string Method2(string from)
        {
            return "这是从第一个插件里面来的数据:" + from;
        }

        public override void Unload()
        {
            Console.WriteLine("Unload" + this.GetType().FullName);
        }
    }
}
